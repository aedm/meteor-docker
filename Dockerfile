FROM node:4.4.7-wheezy

ADD . /app
WORKDIR /app

CMD node main.js
